<!DOCTYPE html>
<html>
	<head>
        <title>Register Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container myrow-container">
			<%
			String warn = (request.getAttribute("Warning") == null) ? "" : (String) request.getAttribute("Warning");
			%>
	   		<div class="panel panel-success">
	           	<div class="panel-heading">
	        		<h2>Please register first and enjoy!</h2>
	        	</div>
	        	<div class="panel-body" style="background-color:lavender;">
	        		<form action="RegisterNewPlayerServlet" method="post">
	        			<div class="col-xs-4">
					      <label for="user">Name:</label>
					      <input type="text" class="form-control" name="user" placeholder="Please enter your name">
					    </div>
					    <div class="col-xs-4">
					      <label for="password">Password:</label>
					      <input type="password" class="form-control" name="password" placeholder="Please enter your password">
					    </div>
					    <div class="col-xs-6">	
					    	<p><%=warn%></p>					
							<input type="submit" class="btn btn-success" value="Register">
							<a class="btn btn-default" href="/RenjuWebGame/index.jsp">Back to Login Page</a>
						</div>
					</form>					
	        	</div>
	        </div>
	    </div>
	</body>
</html>
