<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="com.softserveinc.ita.multigame.model.Player"%>
<%@ page import="com.softserveinc.ita.multigame.model.Gender"%>
<%@ page import="com.softserveinc.ita.multigame.model.GameHistory"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Player profile</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<div class="container myrow-container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<p>
					<%
						Player currentPlayer = (Player) request.getSession().getAttribute("player");
						out.print("Current player: " + String.format("<a href=\"/RenjuWebGame/player?id=%s\">%s</a>",
								currentPlayer.getId(), currentPlayer.getLogin()));
					%>
				</p>
				<%
					Player profilePlayer = (Player)request.getAttribute("player");
					out.print(
							String.format("<h2>Player #%s (%s)<h2>", profilePlayer.getId(), profilePlayer.getLogin())
									);
					
					boolean canEdit = profilePlayer.equals(currentPlayer);					
					%>
			</div>
			<div class="panel-body" style="background-color: lavender;">
				<form action="PlayerProfileController" method="post">
					<input type="hidden" name="id" value="${player.id}">
					<div class="col-xs-6">
						<label for="email">Email:</label> <input type="text"
							class="form-control" name="email" value="${player.email}"
							placeholder="Please enter your email"
							<% if (!canEdit) { %> disabled <% } %>>
					</div>
					<div class="col-xs-6">
						<label for="fullName">Full Name:</label> <input type="text"
							class="form-control" name="fullName" value="${player.fullName}"
							placeholder="Please enter your Full Name"
							<% if (!canEdit) { %> disabled <% } %>>
					</div>
					<div class="col-xs-6" style="padding: 10px;">
						<label for="gender">Gender:</label>
						<div class="radio">
							<%
								if (profilePlayer.getGender() == Gender.MALE) {
							%>
							<label><input type="radio" name="gender" value="MALE"
								checked <%if (!canEdit) {%> disabled <%}%>>Male</label>
							<%
								} else {
							%>
							<label><input type="radio" name="gender" value="MALE"
								<%if (!canEdit) {%> disabled <%}%>>Male</label>
							<%
								}
							%>
						</div>
						<div class="radio">
							<%
								if (profilePlayer.getGender() == Gender.FEMALE) {
							%>
							<label><input type="radio" name="gender" value="FEMALE"
								checked <%if (!canEdit) {%> disabled <%}%>>Female</label>
							<%
								} else {
							%>
							<label><input type="radio" name="gender" value="FEMALE"
								<%if (!canEdit) {%> disabled <%}%>>Female</label>
							<%
								}
							%>
						</div>
					</div>
					<div class="col-xs-6">
						<label for="birthdayDate">Birthday date:</label> <input
							type="date" class="form-control" name="birthdayDate"
							value="${player.birthdayDate}"
							placeholder="Please enter your Birthday date"
							<% if (!canEdit) { %> disabled <% } %>>
					</div>
					<div class="col-xs-6">
						<label for="registrationTime">Registration time:</label> <input
							type="text" class="form-control"
							name="registrationTime" value="${registrationTime}"
							disabled>
					</div>
					<div class="col-xs-12">
						<label for="about">About:</label> <input type="text"
							class="form-control" name="about" value="${player.about}"
							placeholder="Additional information"
							<% if (!canEdit) { %> disabled <% } %>>
					</div>
					<div class="col-xs-12" style="padding: 10px;">
						<% if (profilePlayer.equals(currentPlayer)) { %>
							<input type="submit" class="btn btn-success" value="Save changes">
						<% }%>
						<input type="button" class="btn btn-default" id="ToGameList"
							style="float: right;" value="Back to Game List"
							onclick="location.href='${pageContext.request.contextPath}/GameListController'" />
					</div>
					<div class="col-sm-6">
						<h2>Finished games:</h2>
						<ol>
							<%
								for (GameHistory game : (List<GameHistory>) request.getAttribute("playerGamesList")) {
									out.print(String.format(
											"<li><a href=\"/RenjuWebGame/game_history?id=%s\">Game History # %s (%s vs %s)</a></li>", game.getId(),
											game.getId(), (game.getFirstPlayer() == null) ? "" : game.getFirstPlayer().getLogin(),
											(game.getSecondPlayer() == null) ? "" : game.getSecondPlayer().getLogin()));
								}
							%>
						</ol>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>