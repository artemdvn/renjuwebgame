<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.List" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="com.softserveinc.ita.multigame.model.GameHistory" %>
<%@ page import="com.softserveinc.ita.multigame.model.Player" %>
<%@ page import="com.softserveinc.ita.multigame.model.TurnHistory" %>
<%@ page import="com.softserveinc.ita.multigame.web.services.GsonParserService" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Game history</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		
		<style>
		table, td, th, tr {
		    background-color: burlywood;
		    text-align: center; 
		    font-size: 16px;
		    border: 1px solid grey; 
		    vertical-align:middle;
		}		
		</style>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		
	</head>
	<body>
		<div class="container myrow-container">
			<div class="panel panel-success">
				<div class="panel-heading">	
				<%
					Player currentPlayer = (Player) request.getSession().getAttribute("player");
					out.print("Current player: " 
							+ String.format("<a href=\"/RenjuWebGame/player?id=%s\">%s</a>", currentPlayer.getId(), currentPlayer.getLogin()));
					
					GameHistory currentGame = (GameHistory)request.getAttribute("gameHistory");
					Player firstPlayer = currentGame.getFirstPlayer();
					Player secondPlayer = currentGame.getSecondPlayer();
					
					String winner = (String)request.getAttribute("winner");
					
					
					String startTime = (String)request.getAttribute("startTime");
					String endTime = (String)request.getAttribute("endTime");
					
					out.print(
							String.format("<h2>Game history #%s<h2>", currentGame.getId())
									);
					%>
				</div>
				
				<div class="panel-body" style="background-color:lavender;">
					<div class="col-sm-6">
						<div>
						<%
						if (firstPlayer != null){
							out.print(
									String.format("<h3>First player: <a href=\"/RenjuWebGame/player?id=%s\">%s</a></h3>", firstPlayer.getId(), firstPlayer.getLogin())
									);
						}
						if (secondPlayer != null){
							out.print(
									String.format("<h3>Second player: <a href=\"/RenjuWebGame/player?id=%s\">%s</a></h3>", secondPlayer.getId(), secondPlayer.getLogin())
									);
						}
						out.print("<h3>Start time: " + startTime + "</h3>");
						out.print("<h3>End time: " + endTime + "</h3>");
						out.print("<h3>Winner: " + winner + "</h3>");
						%>
						</div>
					</div>
					
					<div class="col-sm-6" id="turnList">
						<h3>Turn list:</h3>
						<table>
							<thead>
								<tr>
									<th>Date</th>
									<th>Player</th>
									<th>Turn</th>
								</tr>
							</thead>
							<tbody>
							<%
								for (String turn : currentGame.getTurnList()) {
									TurnHistory th = (TurnHistory) GsonParserService.fromJson(turn, TurnHistory.class);
		
									Player p;
									if (th.getPlayerId().equals(currentGame.getFirstPlayer().getId())) {
										p = currentGame.getFirstPlayer();
									} else {
										p = currentGame.getSecondPlayer();
									}
									%>
		
									<tr>
										<td width="200" height="32"><%=th.getTime().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"))%></td>
										<td width="100"><%=p.getLogin()%></td>
										<td width="100"><%=th.getTurn()%></td>
									</tr>
							<% } %>
							</tbody>
						</table>
					</div>
					
					<div class="col-sm-12">
						<form action="${pageContext.request.contextPath}/GameListController" method="post">
						    <input type="submit" id="ToGameList" class="btn btn-default" style="float: left; margin-top: 10px;" value="Back to Game List"/>
						</form>
					</div>	
					
				</div>			
	
			</div>
			    		
		</div>
		
	</body>	
	
</html>