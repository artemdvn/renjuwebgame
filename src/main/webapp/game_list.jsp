<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.softserveinc.ita.multigame.model.Player" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Game list</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container myrow-container">
			<div class="panel panel-success">
				<div class="panel-heading">
		        	<p>
					<%
					Player currentPlayer = (Player) request.getSession().getAttribute("player");
					out.print("Current player: " 
							+ String.format("<a href=\"/RenjuWebGame/player?id=%s\">%s</a>", currentPlayer.getId(), currentPlayer.getLogin()));
					%>
					</p>
					<form action="${pageContext.request.contextPath}/LogoutServlet" method="post">
						<input type="submit" id="ChangePlayer" class="btn btn-default" style="float: right;" value="Change player"/>
					</form>
				</div>
		        
		        <div class="panel-body" style="background-color:lavender;">							
					<div class="row" style="background-color:lavender; text-color:black; float: center;">
					    <div class="col-sm-4">
					    	<h2>Playing games</h2>
					    	<ol>
							<%	
							for (Long id : (List<Long>)request.getAttribute("playingGamesList")) {
								out.print(String.format("<li><a href=\"/RenjuWebGame/game?id=%s\">Game # %s</a></li>", id, id));	
							}
							%>
							</ol>
					    </div>
					    <div class="col-sm-4" style="background-color:lavenderblush;">
						    <h2>Created games</h2>	
							<ol>
							<%	
							for (Long id : (List<Long>)request.getAttribute("createdGamesList")) {
								out.print(String.format("<li><a href=\"/RenjuWebGame/game?id=%s\">Game # %s</a></li>", id, id));	
							}
							%>
							</ol>						
							<form action="CreateNewGameServlet" method="post">
								<input type="submit" class="btn btn-success" value="Create new game">
							</form>
					    </div>
					    <div class="col-sm-4">
					    	<h2>Waiting games</h2>	
							<ol>
							<%	
							for (Long id : (List<Long>)request.getAttribute("waitingGamesList")) {
								out.print(String.format("<li><a href=\"/RenjuWebGame/game?id=%s\">Game # %s</a></li>", id, id));	
							}
							%>
							</ol>
					    </div>			    
				    	
					</div> 
				
					<form action="${pageContext.request.contextPath}/GameListController" method="post">
						<input type="submit" id="Refresh" class="btn btn-primary" value="Refresh"/>
					</form>	
					
				</div>
				
			</div>
		</div>
	</body>
</html>