<!DOCTYPE html>
<html>
	<head>
        <title>Error</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	</head>
	<body>
		 <h1>Error</h1>
	    <div id="loginBox">
		    <p class="error">Login error</p>
		    <p>Back <strong><a href="LoginServlet">to login page</a></strong>.</p>
	    </div>
	</body>
</html>
