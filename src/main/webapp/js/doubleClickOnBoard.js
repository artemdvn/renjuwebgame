window.addEventListener('dblclick', ondblclick, false);

function ondblclick(){
$('#board').dblclick(function(e){
	var gameId = document.getElementById('gameId').value;
    var xClick = e.pageX - $(this).offset().left;
    var yClick = e.pageY - $(this).offset().top;
    var leftBorder = 48;
    var cellWidth = 32;
    var topBorder = 18;
    var cellHeight = 32;
    var boardCol = Math.floor((xClick - leftBorder) / cellWidth);
    var boardRow = Math.floor((yClick - topBorder) / cellHeight);
		    
    var urlNewTurn = 'newTurn?gameId=' + gameId + '&row=' + boardRow + '&col=' + boardCol;
	    
    var ajax = new XMLHttpRequest();
    ajax.open('GET', urlNewTurn, true);
    ajax.send();
		    
    refreshBoard();
    refreshPlayerTurn();
});
}