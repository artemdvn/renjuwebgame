$(document).ready(function() {
	refreshBoard();
	refreshPlayerTurn();
	setInterval(function() {
		refreshBoard()
		refreshPlayerTurn();
		}, 3000); //3 seconds
	});

function refreshBoard() {
	$.ajax({
		method: 'GET',
		url: 'GetBoardController',
		data: 'gameId=' + $('#gameId').val(),
		success: function (data) {
			$('#board').html('<table>');
			$('#board').append('<thead>');
			$('#board').append('<tr>');
			$('#board').append('<th>#</th>');
			$.each(data, function (i, elem) {
				$('#board').append('<th>' + i + '</th>');
				});
			$('#board').append('</tr>');
			$('#board').append('</thead>');
			$('#board').append('<tbody>');
			$.each(data, function (i, elem) {
				$('#board').append('<tr>');
				$('#board').append('<td width="32" style="vertical-align:middle;"><b>' + i + '</b></td>');
				$.each(elem, function (j, inner) {
					if (inner == "B"){
						$('#board').append('<td width="32" height="32" style="border: 1px solid grey; vertical-align:middle;">'
								+ '<img width="30" height="30" src="resources/black_stone.jpg">'
								+ '</td>');
					} else if (inner == "W"){
						$('#board').append('<td width="32" height="32" style="border: 1px solid grey; vertical-align:middle;">'
					       		+ '<img width="30" height="30" src="resources/white_stone.jpg">'
					       		+ '</td>');
					} else{
						$('#board').append('<td width="32" height="32" style="border: 1px solid grey; vertical-align:middle;">'
								+ inner + '</td>');
					}				            					                    
				});
			$('#board').append('</tr>');           
				            });
		            	$('#board').append('</tbody>');
		            $('#board').append('</table>');       	
	            	
				},
	            error: function (errorThrown) {
	                alert("Error: " + errorThrown);
	            }
	        });
	    }

function refreshPlayerTurn() {
	$
			.ajax({
				method : 'GET',
				url : 'GetPlayerTurnController',
				data: 'gameId=' + $('#gameId').val(),
				success : function(data) {
					$('#playerTurn').html(
							'<h2 style="color:red">'
									+ JSON.stringify(data).substring(1,
											JSON.stringify(data).length - 1)
									+ '</h2>');
				},
				error : function(errorThrown) {
					alert("Error: " + errorThrown);
				}
			});
}