<!DOCTYPE html>
<html>
	<head>
        <title>Login Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container myrow-container">
			<%
			String warn = (request.getAttribute("Warning") == null) ? "" : (String) request.getAttribute("Warning");
			%>
	   		<div class="panel panel-success">
	           	<div class="panel-heading">
	        		<h2>Welcome to Renju Web Game!</h2>
	        		<div class="container">
		        	    <img src="${pageContext.request.contextPath}/resources/renju_board.jpg" class="img-rounded" alt="Renju Game" style="float:left; margin-right: 20px;" width="240" height="160">
		        	    <h4>Renju is an abstract strategy board game. 
		        	    <br>
		        	     The game is played with black and white stones on a 15�15 gridded Go board.
		        	    <br>
		        	    The winner is the first player to get an unbroken row of five stones horizontally, vertically, or diagonally.
		        	    </h4>
					</div>
				</div>
	        	<div class="panel-body" style="background-color:lavender;">
	        		<form action="LoginServlet" method="post">
	        			<div class="col-xs-4">
					      <label for="user">Name:</label>
					      <input type="text" class="form-control" name="user" placeholder="Please enter your name">
					    </div>
					    <div class="col-xs-4">
					      <label for="password">Password:</label>
					      <input type="password" class="form-control" name="password" placeholder="Please enter your password">
					    </div>
						<div class="col-xs-6">
							<p><%=warn%></p>
					      	<input type="submit" class="btn btn-success" value="Login">
							<a class="btn btn-default" href="/RenjuWebGame/register.jsp">Register</a>
					    </div>
						
					</form>
	        	</div>
	        </div>
	    </div>
	</body>
</html>
