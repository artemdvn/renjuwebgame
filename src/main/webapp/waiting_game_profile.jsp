<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.softserveinc.ita.multigame.model.PlayingGame" %>
<%@ page import="com.softserveinc.ita.multigame.model.Player" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Game profile</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	</head>
	<body>
		<div class="container myrow-container">
			<div class="panel panel-success">
				<div class="panel-heading">	
					<%
					Player currentPlayer = (Player) request.getSession().getAttribute("player");
					out.print("Current player: " 
							+ String.format("<a href=\"/RenjuWebGame/player?id=%s\">%s</a>", currentPlayer.getId(), currentPlayer.getLogin()));
					
					PlayingGame currentGame = (PlayingGame)request.getAttribute("game");
					Player firstPlayer = currentGame.getGameEngine().getFirstPlayer();
					Player secondPlayer = currentGame.getGameEngine().getSecondPlayer();
					
					out.print(
							String.format("<h2>Game #%s<h2>", currentGame.getGameEngine().getId())
									);
					%>
				</div>
				
				<div class="panel-body" style="background-color:lavender;">
					<%
					if (firstPlayer != null){
						out.print(
								String.format("<h2>First player: <a href=\"/RenjuWebGame/player?id=%s\">%s</a></h2>", firstPlayer.getId(), firstPlayer.getLogin())
								);
					}
					if (secondPlayer != null){
						out.print(
								String.format("<h2>Second player: <a href=\"/RenjuWebGame/player?id=%s\">%s</a></h2>", secondPlayer.getId(), secondPlayer.getLogin())
								);
					} else {
						out.print("<h3>You can connect to this game. Press 'Connect' and enjoy!</h3>");
					}
					%>
		
					<form action="ConnectToWaitingGameServlet" method="post">
						<input type="hidden" name="gameID" value="<%=currentGame.getGameEngine().getId()%>" />
						<input type="submit" id="Connect" class="btn btn-success" value="Connect">
					</form>
					
					<form action="${pageContext.request.contextPath}/game" method="get">
						<input type="hidden" name="id" value="<%=currentGame.getGameEngine().getId()%>" />
						<input type="submit" id="Refresh" class="btn btn-primary" style="margin-top: 10px;" value="Refresh"/>
					</form>
					
					<form action="${pageContext.request.contextPath}/GameListController" method="post">
					    <input type="submit" id="ToGameList" class="btn btn-default" style="float: right;" value="Back to Game List"/>
					</form>
				</div>
	
			</div>
		</div>
		
	</body>
</html>