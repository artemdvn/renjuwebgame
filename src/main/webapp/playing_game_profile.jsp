<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="com.softserveinc.ita.multigame.model.PlayingGame" %>
<%@ page import="com.softserveinc.ita.multigame.model.Player" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Game profile</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		
		<style>
		table, td, th, tr, #board {
		    background-color: burlywood;
		    text-align: center; 
		    font-size: 12px;
		}
		table, #board {
		    table-layout: fixed;
		    width: 550px;
		}		
		</style>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="js/refreshBoard.js"></script>
		<script src="js/doubleClickOnBoard.js"></script>
		
	</head>
	<body>
		<div class="container myrow-container">
			<div class="panel panel-success">
				<div class="panel-heading">
				<%
					Player currentPlayer = (Player) request.getSession().getAttribute("player");
					out.print("Current player: " 
							+ String.format("<a href=\"/RenjuWebGame/player?id=%s\">%s</a>", currentPlayer.getId(), currentPlayer.getLogin()));
					
					PlayingGame currentGame = (PlayingGame)request.getAttribute("game");
					Player firstPlayer = currentGame.getGameEngine().getFirstPlayer();
					Player secondPlayer = currentGame.getGameEngine().getSecondPlayer();					
				%>
				<h2>Game #${gameId}</h2>
				</div>
				
				<div class="panel-body" style="background-color:lavender;">
					<div class="col-sm-6">
						<div>
						<%
						if (firstPlayer != null){
							out.print(
									String.format("<h2>First player: <a href=\"/RenjuWebGame/player?id=%s\">%s</a></h2>", firstPlayer.getId(), firstPlayer.getLogin())
									);
						}
						if (secondPlayer != null){
							out.print(
									String.format("<h2>Second player: <a href=\"/RenjuWebGame/player?id=%s\">%s</a></h2>", secondPlayer.getId(), secondPlayer.getLogin())
									);
						}
						%>
						</div>
						<div id="playerTurn">
						</div>

					<form action="MakeGameTurnServlet" method="post">
						<input id="gameId" type="hidden" name="gameID"
							value="${gameId}" /> 
						<label
							for="turn" style="margin-top: 30px;">Your turn:</label> 
						<input
							type="text" class="form-control" name="turn"
							placeholder="Please make your turn in 'row,col' format">
						<input type="submit" class="btn btn-success"
							style="margin-top: 10px;" value="Make turn">
					</form>
					
					<form action="${pageContext.request.contextPath}/GameListController" method="post">
					    <input type="submit" id="ToGameList" class="btn btn-default" style="float: left; margin-top: 10px;" value="Back to Game List"/>
					</form>

				</div>
		
					<div class="col-sm-6" id="board" style="overflow-x:auto;">						
					</div>
					
				</div>
	
			</div>
			    		
		</div>
		
	</body>

</html>