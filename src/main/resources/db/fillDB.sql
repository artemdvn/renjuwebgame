DELETE FROM players;
ALTER SEQUENCE global_seq RESTART WITH 1;

INSERT INTO players (login, password, email)
VALUES ('Tom', 'tom', 'tom@gmail.com');

INSERT INTO players (login, password, email, gender)
VALUES ('Alex', 'alex', 'alex@gmail.com', 'MALE');

INSERT INTO players (login, password, email, gender, fullname)
VALUES ('Ivan', 'ivan', 'ivan@gmail.com', 'MALE', 'Ivan Ivanov');