package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.services.PlayerService;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PlayerService ps = new PlayerService();
	Logger logger = Logger.getLogger(LoginServlet.class);
	
	public void init(ServletConfig config) throws ServletException {  
	    super.init(config);
	}
       
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	String user = request.getParameter("user");
    	String password = request.getParameter("password");
    			
    	Player player = ps.getPlayerByNameAndPassword(user, password);
    	
		if (player != null) {
			request.getSession().setAttribute("player", player);
			logger.info(String.format("player %s started new session", player.getLogin()));
			request.getRequestDispatcher("/GameListController").forward(request, response);
		} else {			
			request.getRequestDispatcher("register.jsp").forward(request, response);
		}
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		doGet(request, response);
	}

}
