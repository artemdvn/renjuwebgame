package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayingGame;
import com.softserveinc.ita.multigame.web.services.AllGameListsService;

@WebServlet("/GetPlayerTurnController")
public class GetPlayerTurnController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AllGameListsService agls = new AllGameListsService();
	
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
    	
    	PlayingGame currentGame = agls.getGameById(Long.valueOf(request.getParameter("gameId")));
		Player firstPlayer = currentGame.getGameEngine().getFirstPlayer();
		Player secondPlayer = currentGame.getGameEngine().getSecondPlayer();
		
		int gameState = currentGame.getGameEngine().getGameState();
		String gameStateToShow = "";
		switch (gameState) {
		case 3:
			gameStateToShow = "waiting while " + firstPlayer.getLogin() + " makes turn...";
			break;
		case 4:
			gameStateToShow = "waiting while " + secondPlayer.getLogin() + " makes turn...";
			break;
		case 5:
			gameStateToShow = "The winner is " + firstPlayer.getLogin() + "!";
			break;
		case 6:
			gameStateToShow = "The winner is " + secondPlayer.getLogin() + "!";
			break;
		case 7:
			gameStateToShow = "Draw!";
			break;
		}
		
		Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();

        String jsonGameState = gson.toJson(gameStateToShow);
		
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonGameState);
    }
}
