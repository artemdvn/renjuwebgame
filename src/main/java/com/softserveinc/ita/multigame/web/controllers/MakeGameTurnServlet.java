package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.services.MakeTurnService;

@WebServlet("/MakeGameTurnServlet")
public class MakeGameTurnServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MakeTurnService mts = new MakeTurnService();

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String turn = request.getParameter("turn");		
		Player player = (Player) request.getSession().getAttribute("player");		
		Long id = Long.parseLong(request.getParameter("gameID"));
		
		request.setAttribute("game", mts.makeTurn(player, id, turn));
		request.setAttribute("gameId", id);
		request.setAttribute("player", player);
		
		request.getRequestDispatcher("playing_game_profile.jsp").forward(request, response);

	}
}
