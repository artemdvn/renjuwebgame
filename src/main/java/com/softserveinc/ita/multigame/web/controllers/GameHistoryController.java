package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.ResultStatus;
import com.softserveinc.ita.multigame.web.services.GameHistoryService;

@WebServlet("/game_history/*")
public class GameHistoryController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private GameHistoryService ghs = new GameHistoryService(); 
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Long id = Long.parseLong(request.getParameter("id"));
		Player player = (Player) request.getSession().getAttribute("player");
				
		GameHistory game = ghs.get(id);
		
		ResultStatus resultStatus = game.getResultStatus();
		String winner = "";
		switch (resultStatus) {
			case FIRST_PLAYER_WINS:
				winner = game.getFirstPlayer().getLogin();
				break;
			case SECOND_PLAYER_WINS:
				winner = game.getSecondPlayer().getLogin();
				break;
			case DRAWN:
				winner = "Draw";
		}
		
		request.setAttribute("gameHistory", game);		
		request.setAttribute("player", player);
		request.setAttribute("winner", winner);
		request.setAttribute("startTime", game.getStartTime().format(formatter));
		request.setAttribute("endTime", game.getEndTime().format(formatter));
		
		request.getRequestDispatcher("game_history.jsp").forward(request, response);
		
	}

}
