package com.softserveinc.ita.multigame.web.services;

import java.time.LocalDateTime;
import java.util.List;

import org.jasypt.util.password.StrongPasswordEncryptor;

import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.dao.PlayerDaoImpl;
import com.softserveinc.ita.multigame.model.Player;

public class PlayerService {
	
	private PlayerDao pm = PlayerDaoImpl.getInstance();
	
	public boolean registerNewPlayer(String name, String password) {
		if (name == null) {
			return false;
		}
		if (password == null) {
			return false;
		}
		if (name.length() == 0) {
			return false;
		}
		if (password.length() == 0) {
			return false;
		}
		Player player = new Player(name, getEncryptedPassword(password));
		player.setRegistrationTime(LocalDateTime.now());
		pm.create(player);
		return true;
	}
	
	public boolean contains(Player player) {
		if (player == null) {
			return false;
		}
		return (pm.get(player.getId()) == null) ? false : true;
	}
	
	public Player getPlayerByNameAndPassword(String name, String password) {
		StrongPasswordEncryptor passwordEncryptorStrong = new StrongPasswordEncryptor();
		List<Player> players = pm.getAll();
		for (Player player : players) {
			if (player.getLogin().equals(name)
					&& passwordEncryptorStrong.checkPassword(password, player.getPassword())) {
				return player;
			}
		}
		return null;
	}
	
	public Player getPlayerById(Long id) {
		return pm.get(id);
	}
	
	public void update(Player player) {
		pm.update(player);
	}
	
	public void setPlayerDao(PlayerDao playerDao) {
        this.pm = playerDao;
    }
	
	private String getEncryptedPassword(String originalPassword) {
		StrongPasswordEncryptor passwordEncryptorStrong = new StrongPasswordEncryptor();
		return passwordEncryptorStrong.encryptPassword(originalPassword);
	}
	
}
