package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.services.PlayerService;

@WebServlet("/RegisterNewPlayerServlet")
public class RegisterNewPlayerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PlayerService ps = new PlayerService();
	private String warning = "";
	Logger logger = Logger.getLogger(RegisterNewPlayerServlet.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String user = request.getParameter("user");
		String password = request.getParameter("password");

		Player player = ps.getPlayerByNameAndPassword(user, password);

		if (player == null) {
			if (ps.registerNewPlayer(user, password)) {
				warning = "<font color=green>Success! New player registered! Now you can login and play!</font>";
				logger.info(String.format("new player %s registered", user));
			} else {
				warning = "<font color=red>Registration error! Check player name!</font>";
				logger.error(String.format("player %s registration error", user));
			}
		} else {
			warning = "<font color=red>This player already exists! Check player name or just login and play!</font>";
			logger.warn(String.format("player %s already exists", user));
		}

		request.setAttribute("Warning", warning);
		request.setAttribute("user", user);
		request.setAttribute("password", password);
		request.getRequestDispatcher("register.jsp").forward(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

}
