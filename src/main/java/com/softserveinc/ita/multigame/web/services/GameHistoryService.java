package com.softserveinc.ita.multigame.web.services;

import java.util.List;

import com.softserveinc.ita.multigame.dao.GameHistoryDao;
import com.softserveinc.ita.multigame.model.GameHistory;

public class GameHistoryService {

	private GameHistoryDao gameHistoryDao = GameHistoryDao.getInstance();

	public void create(GameHistory gameHistory) {
		gameHistoryDao.create(gameHistory);
	}

	public void update(GameHistory gameHistory) {
		gameHistoryDao.update(gameHistory);
	}

	public void delete(Long id) {
		gameHistoryDao.delete(id);
	}

	public GameHistory get(Long gameId) {
		return gameHistoryDao.get(gameId);
	}

	public List<GameHistory> getAll() {
		return gameHistoryDao.getAll();
	}
	
	public List<GameHistory> getPlayerGames(Long id) {
        return gameHistoryDao.getHistoriesByPlayerId(id);
    }
	
	public void setGameHistoryDao(GameHistoryDao gameHistoryDao) {
        this.gameHistoryDao = gameHistoryDao;
    }

}
