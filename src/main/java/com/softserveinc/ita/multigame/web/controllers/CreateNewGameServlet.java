package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.services.AllGameListsService;

@WebServlet("/CreateNewGameServlet")
public class CreateNewGameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AllGameListsService agls = new AllGameListsService();
	Logger logger = Logger.getLogger(CreateNewGameServlet.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Player player = (Player) request.getSession().getAttribute("player");
		
		if (player != null) {
			
			agls.createGame(player);
			
			logger.info(String.format("player %s created new game", player.getLogin()));
			
			request.setAttribute("playingGamesList", agls.getPlayingGameIDs(player));
			request.setAttribute("createdGamesList", agls.getCreatedGameIDs(player));
			request.setAttribute("waitingGamesList", agls.getWaitingGameIDs(player));
			
			request.getRequestDispatcher("/GameListController").forward(request, response);
		}
	}
}
