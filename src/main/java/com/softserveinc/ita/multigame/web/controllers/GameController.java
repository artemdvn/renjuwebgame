package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayingGame;
import com.softserveinc.ita.multigame.web.services.AllGameListsService;

@WebServlet("/game/*")
public class GameController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AllGameListsService agls = new AllGameListsService();   

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Long id = Long.parseLong(request.getParameter("id"));
		Player player = (Player) request.getSession().getAttribute("player");
		
		PlayingGame game = agls.getGameById(id);
		
		List<Long> playingGamesList = agls.getPlayingGameIDs(player);
		List<Long> createdGamesList = agls.getCreatedGameIDs(player);
		List<Long> waitingGamesList = agls.getWaitingGameIDs(player);
		
		request.setAttribute("game", game);	
		request.setAttribute("gameId", id);
		request.setAttribute("player", player);
		
		if (playingGamesList.contains(id)) {
			request.getRequestDispatcher("playing_game_profile.jsp").forward(request, response);
		} else if (createdGamesList.contains(id)) {
			request.getRequestDispatcher("created_game_profile.jsp").forward(request, response);
		} else if (waitingGamesList.contains(id)) {
			request.getRequestDispatcher("waiting_game_profile.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("playing_game_profile.jsp").forward(request, response);
		}
		
	}

}
