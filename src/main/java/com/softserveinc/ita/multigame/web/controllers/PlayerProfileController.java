package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Gender;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.services.GameHistoryService;
import com.softserveinc.ita.multigame.web.services.PlayerService;

@WebServlet(urlPatterns = { "/player/*", "/PlayerProfileController" })
public class PlayerProfileController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PlayerService ps = new PlayerService();
	private GameHistoryService ghs = new GameHistoryService();
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Long id = Long.parseLong(request.getParameter("id"));

		Player player = ps.getPlayerById(id);

		List<GameHistory> playerGamesList = ghs.getPlayerGames(id);
		
		request.setAttribute("id", id);
		request.setAttribute("player", player);
		request.setAttribute("playerGamesList", playerGamesList);
		request.setAttribute("registrationTime",
				(player.getRegistrationTime() == null) ? "" : player.getRegistrationTime().format(formatter));
		request.getRequestDispatcher("player_profile.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Long id = Long.parseLong(request.getParameter("id"));

		Player player = ps.getPlayerById(id);

		player.setEmail(request.getParameter("email"));
		player.setFullName(request.getParameter("fullName"));
		String gender = request.getParameter("gender");
		if (gender != null && !gender.isEmpty()) {
			player.setGender(Gender.valueOf(gender));
		}
		String birthdayDate = request.getParameter("birthdayDate");
		if (birthdayDate != null && !birthdayDate.isEmpty()) {
			player.setBirthdayDate(LocalDate.parse(birthdayDate));
		}
		player.setAbout(request.getParameter("about"));

		ps.update(player);

		doGet(request, response);

	}

}
