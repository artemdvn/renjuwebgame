package com.softserveinc.ita.multigame.web.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonParserService {
    public static String makeJson(Object object) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(object);
    }

    public static Object fromJson(String jsonString, Class myClass) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, myClass);
    }
}
