package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayingGame;
import com.softserveinc.ita.multigame.web.services.AllGameListsService;

@WebServlet("/ConnectToWaitingGameServlet")
public class ConnectToWaitingGameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AllGameListsService agls = new AllGameListsService();
	Logger logger = Logger.getLogger(ConnectToWaitingGameServlet.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Player player = (Player) request.getSession().getAttribute("player");
		
		Long id = Long.parseLong(request.getParameter("gameID"));
		
		PlayingGame game = agls.connectToWaitingGame(id, player);
		
		logger.info(String.format("player %s connected to game %s", player.getLogin(), id));
		
		request.setAttribute("game", game);
		request.setAttribute("gameId", game.getGameEngine().getId());
		request.setAttribute("player", player);
		request.getRequestDispatcher("playing_game_profile.jsp").forward(request, response);

	}
}
