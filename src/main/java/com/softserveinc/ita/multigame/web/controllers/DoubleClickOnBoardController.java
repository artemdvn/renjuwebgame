package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.services.MakeTurnService;

@WebServlet("/newTurn")
public class DoubleClickOnBoardController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MakeTurnService mts = new MakeTurnService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String row = request.getParameter("row");		
		String col = request.getParameter("col");
		String turn = row + "," + col;
		Player player = (Player) request.getSession().getAttribute("player");		
		Long id = Long.parseLong(request.getParameter("gameId"));
		
		request.setAttribute("game", mts.makeTurn(player, id, turn));		
		request.setAttribute("player", player);
		request.getRequestDispatcher("playing_game_profile.jsp").forward(request, response);

	}
}
