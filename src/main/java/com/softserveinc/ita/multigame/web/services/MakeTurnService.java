package com.softserveinc.ita.multigame.web.services;

import java.time.LocalDateTime;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayingGame;
import com.softserveinc.ita.multigame.model.ResultStatus;
import com.softserveinc.ita.multigame.model.TurnHistory;
import com.softserveinc.ita.multigame.model.engine.GameState;
import com.softserveinc.ita.multigame.model.engine.renju.RenjuGame;

public class MakeTurnService {

	private AllGameListsService agls = new AllGameListsService();
	private GameHistoryService ghs = new GameHistoryService();

	public PlayingGame makeTurn(Player player, Long gameID, String turn) {
		PlayingGame game = agls.getGameById(gameID);

		if (game.getGameEngine().makeTurn(player, turn)) {

			game.getTurnList().add(convertTurnToJson(gameID, player.getId(), turn));

			if (game.getGameEngine().isFinished()) {
				saveGameHistory(game);
			}
		}

		return game;
	}

	private String convertTurnToJson(Long gameID, Long playerId, String turn) {
		TurnHistory th = new TurnHistory(LocalDateTime.now(), playerId, gameID, turn);
		return GsonParserService.makeJson(th);
	}

	private void saveGameHistory(PlayingGame game) {		
		ResultStatus resultStatus = null;
		RenjuGame rg = game.getGameEngine();
		
		switch (rg.getGameState()) {
		case GameState.FINISHED_WITH_FIRST_PLAYER_AS_A_WINNER:
			resultStatus = ResultStatus.FIRST_PLAYER_WINS;
			break;
		case GameState.FINISHED_WITH_SECOND_PLAYER_AS_A_WINNER:
			resultStatus = ResultStatus.SECOND_PLAYER_WINS;
			break;
		case GameState.FINISHED_WITH_DRAW:
			resultStatus = ResultStatus.DRAWN;
			break;
		}

		GameHistory gameHistory = new GameHistory(rg.getId(), rg.getFirstPlayer(), rg.getSecondPlayer(),
				game.getStartTime(), LocalDateTime.now(), game.getTurnList(), resultStatus);
		
		System.out.println(game.getTurnList());
		ghs.create(gameHistory);
	}

}
