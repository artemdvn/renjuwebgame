package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.web.services.AllGameListsService;
import com.softserveinc.ita.multigame.web.services.GsonParserService;

@WebServlet("/GetBoardController")
public class GetBoardController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AllGameListsService agls = new AllGameListsService();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String[][] board = agls.getGameById(Long.valueOf(request.getParameter("gameId"))).getGameEngine().getGameBoard();

		String jsonBoard = GsonParserService.makeJson(board);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonBoard);
	}
}
