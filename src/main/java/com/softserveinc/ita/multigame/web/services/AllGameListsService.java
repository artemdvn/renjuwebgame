package com.softserveinc.ita.multigame.web.services;

import java.util.List;

import com.softserveinc.ita.multigame.model.GameListManager;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayingGame;

public class AllGameListsService {
	
	private GameListManager glm = GameListManager.getInstance();
	
	public PlayingGame createGame(Player player) {
		PlayingGame game = glm.createGame(player);
		return game;
	}
	
	public List<Long> getCreatedGameIDs(Player player) {
		return glm.getCreatedGameIDs(player);
	}
	
	public List<Long> getPlayingGameIDs(Player player) {
		return glm.getPlayingGameIDs(player);		
	}
	
	public List<Long> getWaitingGameIDs(Player player) {
		return glm.getWaitingGameIDs(player);
	}
	
	public PlayingGame connectToWaitingGame(Long gameID, Player player) {
		PlayingGame waitingGame = glm.getGameById(gameID);
		if (waitingGame.getGameEngine().getSecondPlayer() == null) {
			waitingGame.getGameEngine().setSecondPlayer(player);
			waitingGame.setStartTime();
		} 
		return waitingGame;
	}
	
	public PlayingGame getGameById(Long id) {
		PlayingGame game = glm.getGameById(id);
		return game;
	}
	
}
