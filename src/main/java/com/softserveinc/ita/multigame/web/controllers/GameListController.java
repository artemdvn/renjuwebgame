package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.services.AllGameListsService;

@WebServlet("/GameListController")
public class GameListController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AllGameListsService agls = new AllGameListsService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Player player = (Player) request.getSession().getAttribute("player");
		
		request.setAttribute("playingGamesList", agls.getPlayingGameIDs(player));
		request.setAttribute("createdGamesList", agls.getCreatedGameIDs(player));
		request.setAttribute("waitingGamesList", agls.getWaitingGameIDs(player));
		
		request.getRequestDispatcher("game_list.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		doGet(request, response);
	}

}
