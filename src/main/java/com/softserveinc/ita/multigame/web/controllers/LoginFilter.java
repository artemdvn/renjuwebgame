package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws ServletException, IOException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession(false);

		String loginURI = request.getContextPath() + "/LoginServlet";
		String logoutURI = request.getContextPath() + "/LogoutServlet";
		String registerURI = request.getContextPath() + "/RegisterNewPlayerServlet";
		String registerJsp = request.getContextPath() + "/register.jsp";
		String resourceURI = request.getContextPath() + "/resources";

		boolean isLoggedIn = session != null && session.getAttribute("player") != null;
		boolean isLoginRequest = request.getRequestURI().startsWith(loginURI);
		boolean isLogoutRequest = request.getRequestURI().startsWith(logoutURI);
		boolean isResourceRequest = request.getRequestURI().startsWith(resourceURI);
		boolean isRegisterRequest = request.getRequestURI().startsWith(registerURI)
				|| request.getRequestURI().startsWith(registerJsp);

		if (isLoggedIn && (isLoginRequest || isRegisterRequest)) {
			response.sendRedirect("/RenjuWebGame/GameListController");
		} else if (isLoggedIn || isLoginRequest || isLogoutRequest || isRegisterRequest || isResourceRequest) {
			chain.doFilter(request, response);
		} else {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

	@Override
	public void destroy() {
	}

}
