package com.softserveinc.ita.multigame.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.softserveinc.ita.multigame.model.engine.renju.RenjuGame;
import com.softserveinc.ita.multigame.web.services.PlayerService;

public class GameListManager {
	private static List<PlayingGame> games;
	private static GameListManager instance = null;
	
	private GameListManager() {
		games = new CopyOnWriteArrayList<PlayingGame>();
		
		PlayerService ps = new PlayerService();
		Player tom = ps.getPlayerByNameAndPassword("Tom", "tom");
		Player alex = ps.getPlayerByNameAndPassword("Alex", "alex");
		Player ivan = ps.getPlayerByNameAndPassword("Ivan", "ivan");
		
		RenjuGame rg = new RenjuGame();
		rg.setFirstPlayer(tom);
		PlayingGame pg = new PlayingGame(rg);
		games.add(pg);
		
		rg = new RenjuGame();
		rg.setFirstPlayer(alex);
		pg = new PlayingGame(rg);
		games.add(pg);
		
		rg = new RenjuGame();
		rg.setFirstPlayer(alex);
		rg.setSecondPlayer(tom);
		
		pg = new PlayingGame(rg);
		pg.setStartTime();
		
		rg.makeTurn(alex, "0,1");
		rg.makeTurn(tom, "8,8");
		rg.makeTurn(alex, "0,2");
		rg.makeTurn(tom, "9,9");
		games.add(pg);
		
		rg = new RenjuGame();
		rg.setFirstPlayer(tom);
		rg.setSecondPlayer(ivan);
		pg = new PlayingGame(rg);
		pg.setStartTime();
		games.add(pg);
	}
	
	public static GameListManager getInstance() {
		if (instance == null) {
			instance = new GameListManager();
		}
		return instance;
	}
	
	public List<Long> getGameIDs() {
		List<Long> ls = new ArrayList<Long>();
		for (PlayingGame game : games){
			ls.add(game.getGameEngine().getId());
		}
		return ls;
	}
	
	public PlayingGame getGameById(Long id) {
		for (PlayingGame game : games){
			if (game.getGameEngine().getId().equals(id)){
				return game;
			}
		}
		return null;
	}
	
	public PlayingGame createGame(Player player) {
		RenjuGame rg = new RenjuGame();
		rg.setFirstPlayer(player);
		PlayingGame game = new PlayingGame(rg);
		games.add(game);
		return game;
	}
	
	public boolean deleteGame(Long id) {
		for (PlayingGame game : games){
			if (game.getGameEngine().getId().equals(id)){
				games.remove(game);
				break;
			}
		}
		return true;
	}
	
	public List<Long> getCreatedGameIDs(Player player) {
		List<Long> ls = new ArrayList<Long>();
		for (PlayingGame game : games) {
			RenjuGame rg = game.getGameEngine();
			if ((rg.getFirstPlayer().equals(player)) 
					&& (rg.getSecondPlayer() == null)) {
				ls.add(rg.getId());
			}
		}
		return ls;
	}
	
	public List<Long> getPlayingGameIDs(Player player) {
		List<Long> ls = new ArrayList<Long>();
		for (PlayingGame game : games) {
			RenjuGame rg = game.getGameEngine();
			if ((rg.getFirstPlayer().equals(player)) 
					&& (rg.getSecondPlayer() != null)
					&& (!rg.isFinished())) {
				ls.add(rg.getId());
			} else if ((rg.getSecondPlayer() != null)
					&& (rg.getSecondPlayer().equals(player))
					&& (!rg.isFinished())) {
				ls.add(rg.getId());
			}
		}
		return ls;
	}
	
	public List<Long> getWaitingGameIDs(Player player) {
		List<Long> ls = new ArrayList<Long>();
		for (PlayingGame game : games) {
			RenjuGame rg = game.getGameEngine();
			if (!(rg.getFirstPlayer().equals(player)) 
					&& (rg.getSecondPlayer() == null)) {
				ls.add(rg.getId());
			}
		}
		return ls;
	}
	
}
