package com.softserveinc.ita.multigame.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "games_history")
public class GameHistory implements Serializable{

    private static final long serialVersionUID = 1L;
    
	private Long id;
    private Player firstPlayer;
    private Player secondPlayer;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private List<String> turnList;
    private ResultStatus resultStatus;

    public GameHistory() {
    }

    public GameHistory(Long id, Player firstPlayer, Player secondPlayer, LocalDateTime startTime, LocalDateTime endTime, List<String> turnList, ResultStatus resultStatus) {
        this.id = id;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.startTime = startTime;
        this.endTime = endTime;
        this.turnList = turnList;
        this.resultStatus = resultStatus;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "first_player_id", nullable = false)
    public Player getFirstPlayer() {
        return firstPlayer;
    }

    public void setFirstPlayer(Player firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "second_player_id", nullable = false)
    public Player getSecondPlayer() {
        return secondPlayer;
    }

    public void setSecondPlayer(Player secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    @NotNull
    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    @NotNull
    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "turn_list",
            joinColumns = @JoinColumn(name = "game_history_id"))
    @NotNull
    public List<String> getTurnList() {
        return turnList;
    }

    public void setTurnList(List<String> turnList) {
        this.turnList = turnList;
    }

    @Enumerated
    @NotNull
    public ResultStatus getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(ResultStatus resultStatus) {
        this.resultStatus = resultStatus;
    }

    @Override
    public boolean equals(Object obj) {
    	if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GameHistory other = (GameHistory) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;

    }

    @Override
    public int hashCode() {
    	final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
    }

    @Override
    public String toString() {
        return "GameHistory{" +
                "id=" + id +
                ", firstPlayer=" + firstPlayer + '\'' +
                ", secondPlayer=" + secondPlayer + '\'' +
                ", startTime=" + startTime + '\'' +
                ", endTime=" + endTime + '\'' +
                ", turnList=" + turnList + '\'' +
                ", resultStatus=" + resultStatus + '\'' +
                '}';
    }
}
