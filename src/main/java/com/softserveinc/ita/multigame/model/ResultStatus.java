package com.softserveinc.ita.multigame.model;

public enum ResultStatus {
	FIRST_PLAYER_WINS, SECOND_PLAYER_WINS, DRAWN
}
