package com.softserveinc.ita.multigame.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.*;

@Entity
@Table(name = "players")
public class Player {
	
	private Long id;
    private String login;
    private String password;
    private String email;
    private String fullName;
    private Gender gender;
    private LocalDate birthdayDate;
    private LocalDateTime registrationTime;
    private String about;
    private String avatar; // keep like a path to DB?
	
	private static Long nextId = 0L;

	public Player() {
    }

    public Player(String login) {
        this.login = login;
    }

	public Player(String login, String password) {
		this(nextId++);
		if (nextId == Long.MAX_VALUE) {
			throw new RuntimeException(String.format("%s has reached maximum ID value. " + "Restart the Application.",
					this.getClass().getSimpleName()));
		}
		this.login = login;
		this.password = password;
	}
	
	public Player(String login, String password, String email) {
        this(login, password);
        this.email = email;
    }
	
	public Player(Long id, String login, String password, String email, String fullName, Gender gender, LocalDate birthdayDate, LocalDateTime registrationTime, String about, String avatar) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
        this.fullName = fullName;
        this.gender = gender;
        this.birthdayDate = birthdayDate;
        this.registrationTime = registrationTime;
        this.about = about;
        this.avatar = avatar;
    }
	
	protected Player(final Long id) {
		this.id = id;
	}
	
	@Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	@Column(name = "login", nullable = false)
	public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    
    @Column(name = "password", nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Enumerated(EnumType.STRING)
    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(LocalDate birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public LocalDateTime getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(LocalDateTime registrationTime) {
        this.registrationTime = registrationTime;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", fullName='" + fullName + '\'' +
                ", gender=" + gender +
                ", birthdayDate=" + birthdayDate +
                ", registrationTime=" + registrationTime +
                ", about='" + about + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }

}
