package com.softserveinc.ita.multigame.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.softserveinc.ita.multigame.model.engine.renju.RenjuGame;

public class PlayingGame {

	private RenjuGame gameEngine;

	private LocalDateTime startTime;
	private List<String> turnList;

	public PlayingGame(RenjuGame gameEngine) {
		this.gameEngine = gameEngine;
		this.turnList = new ArrayList<>();
	}

	public RenjuGame getGameEngine() {
		return gameEngine;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime() {
		if (startTime == null 
				&& gameEngine.getFirstPlayer() != null 
				&& gameEngine.getSecondPlayer() != null) {
			startTime = LocalDateTime.now();
		}
	}

	public List<String> getTurnList() {
		return turnList;
	}

	public void setTurnList(List<String> turnList) {
		this.turnList = turnList;
	}

}
