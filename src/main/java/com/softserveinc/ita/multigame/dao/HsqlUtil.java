package com.softserveinc.ita.multigame.dao;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HsqlUtil {
    private static final SessionFactory sf = configureSF();
    
    private static SessionFactory configureSF() throws HibernateException {
    	try {
			return new Configuration().configure("db/hibernateHsql.cfg.xml").buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
    }

    public static SessionFactory getSessionFactory() {
        return sf;
    }
}