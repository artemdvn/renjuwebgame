package com.softserveinc.ita.multigame.dao;

import java.util.List;

import com.softserveinc.ita.multigame.model.Player;

public interface PlayerDao {

	void create(Player player);

	Player get(Long playerId);

	List<Player> getAll();

	void update(Player player);

	void delete(Player player);

	void delete(Long id);

}
