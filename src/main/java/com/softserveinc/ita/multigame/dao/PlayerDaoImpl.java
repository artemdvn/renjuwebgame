package com.softserveinc.ita.multigame.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.softserveinc.ita.multigame.model.Player;

public class PlayerDaoImpl implements PlayerDao {
	private static PlayerDaoImpl instance = null;
	private static SessionFactory sf = HibernateUtil.getSessionFactory();

	private PlayerDaoImpl() {
	}

	public static PlayerDaoImpl getInstance() {
		if (instance == null) {
			instance = new PlayerDaoImpl();
		}
		return instance;
	}

	public void create(Player player) {
		Session session = sf.openSession();
		session.beginTransaction();
		session.save(player);
		session.getTransaction().commit();
		session.close();
	}

	public Player get(Long playerId) {
		Session session = sf.openSession();
		Player player = session.get(Player.class, playerId);
		session.close();
		return player;
	}

	public List<Player> getAll() {
		Session session = sf.openSession();
		List<Player> players = session.createQuery("from Player").getResultList();
		session.close();
		return players;
	}

	public void update(Player player) {
		Session session = sf.openSession();
		session.beginTransaction();
		session.update(player);
		session.getTransaction().commit();
		session.close();
	}

	public void delete(Player player) {
		Session session = sf.openSession();
		session.beginTransaction();
		session.delete(player);
		session.getTransaction().commit();
		session.close();
	}

	public void delete(Long id) {
		Session session = sf.openSession();
		session.beginTransaction();
		Query query = session.createQuery("delete Player where id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();
	}

}
