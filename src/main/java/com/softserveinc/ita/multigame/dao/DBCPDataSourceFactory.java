package com.softserveinc.ita.multigame.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

public class DBCPDataSourceFactory {

	public static DataSource getDataSource() {
		
		Properties props = new Properties();
		InputStream inputStream = null;
		BasicDataSource ds = new BasicDataSource();

		try {
			inputStream = new DBCPDataSourceFactory().getClass().getClassLoader()
					.getResourceAsStream("db/db.properties");
			props.load(inputStream);
			
			ds.setDriverClassName(props.getProperty("DB_DRIVER_CLASS"));
			ds.setUrl(props.getProperty("DB_URL"));
			ds.setUsername(props.getProperty("DB_USERNAME"));
			ds.setPassword(props.getProperty("DB_PASSWORD"));
			ds.setMaxIdle(30);
			ds.setInitialSize(3);
						
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		return ds;
	}

}
