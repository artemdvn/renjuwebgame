package com.softserveinc.ita.multigame.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import com.softserveinc.ita.multigame.model.GameHistory;

public class GameHistoryDao {
	private static GameHistoryDao instance = null;
	private static SessionFactory sf = HibernateUtil.getSessionFactory();

	private GameHistoryDao() {
	}

	public static GameHistoryDao getInstance() {
		if (instance == null) {
			instance = new GameHistoryDao();
		}
		return instance;
	}
	
	public void create(GameHistory gameHistory) {
		Session session = sf.openSession();
		session.beginTransaction();
		session.save(gameHistory);
		session.getTransaction().commit();
		session.close();
	}

	public void update(GameHistory gameHistory) {
		Session session = sf.openSession();
		session.beginTransaction();
		session.update(gameHistory);
		session.getTransaction().commit();
		session.close();
	}

	public void delete(Long id) {
		Session session = sf.openSession();
		session.beginTransaction();
		Query query = session.createQuery("delete GameHistory where id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();
	}

	public GameHistory get(Long id) {
		Session session = sf.openSession();
		
		Criteria criteria = session.createCriteria(GameHistory.class);
        criteria.add(Restrictions.eq("id", id));
        GameHistory gameHistory = (GameHistory) criteria.uniqueResult();
        
		session.close();
		return gameHistory;
	}

	public List<GameHistory> getAll() {
		Session session = sf.openSession();
		
		Criteria criteria = session.createCriteria(GameHistory.class);
        List<GameHistory> gameHistories = criteria.list();
		
        session.close();
		return gameHistories;
	}
	
	public List<GameHistory> getHistoriesByPlayerId(Long id) {
        Session session = sf.openSession();

        List<GameHistory> gameHistories = new ArrayList<>();
        Query<GameHistory> query = session.createQuery("from GameHistory gh where gh.firstPlayer.id = :id");
        query.setParameter("id", id);
        gameHistories.addAll(query.list());

        Query<GameHistory> query2 = session.createQuery("from GameHistory gh where gh.secondPlayer.id = :id");
        query2.setParameter("id", id);
        gameHistories.addAll(query2.list());

        session.close();
        return gameHistories;
    }

}
