package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.lang.reflect.Field;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.controllers.LoginServlet;
import com.softserveinc.ita.multigame.web.services.PlayerService;

public class TestLoginServlet extends Mockito{
	
	@Mock
	private HttpServletRequest request;	
	@Mock
	private HttpServletResponse response;	
	@Mock
	private HttpSession session;	
	@Mock
	private RequestDispatcher rd;	
	@Mock
    private PlayerService ps;
	@Mock
	private Player second;
	
	private LoginServlet servlet;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		servlet = new LoginServlet();
	    Field f = LoginServlet.class.getDeclaredField("ps");
	    f.setAccessible(true);
	    f.set(servlet, ps);
	}
	
	@Test
	public void testLoginDoPost() throws ServletException, IOException {
		
		when(request.getSession()).thenReturn(session);
		when(request.getParameter("user")).thenReturn("Second");
		when(request.getParameter("password")).thenReturn("second");
		when(ps.getPlayerByNameAndPassword("Second", "second")).thenReturn(second);
		when(request.getRequestDispatcher("/GameListController")).thenReturn(rd);
	    
		servlet.doPost(request, response);
		
		verify(rd).forward(request, response);
		verify(ps).getPlayerByNameAndPassword("Second", "second");
		verify(request).getParameter("user");
		verify(request).getParameter("password");
		
	}

}
