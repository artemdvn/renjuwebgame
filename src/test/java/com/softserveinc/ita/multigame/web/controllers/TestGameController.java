package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayingGame;
import com.softserveinc.ita.multigame.model.engine.renju.RenjuGame;
import com.softserveinc.ita.multigame.web.services.AllGameListsService;

public class TestGameController extends Mockito{
	
	@Mock
	private HttpServletRequest request;	
	@Mock
	private HttpServletResponse response;	
	@Mock
	private HttpSession session;	
	@Mock
	private RequestDispatcher rd;	
	@Mock
    private AllGameListsService agls;
	@Mock
    private PlayingGame game;
	@Mock
    private RenjuGame gameEngine;
	@Mock
	private Player second;
	
	private GameController servlet;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		servlet = new GameController();
	    Field f = GameController.class.getDeclaredField("agls");
	    f.setAccessible(true);
	    f.set(servlet, agls);
	}
	
	@Test
	public void testGameControllerDoGet() throws ServletException, IOException {
		
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("player")).thenReturn(second);
		when(request.getParameter("id")).thenReturn("1");
		when(request.getRequestDispatcher(anyString())).thenReturn(rd);
		when(agls.getPlayingGameIDs(second)).thenReturn(new ArrayList<Long>());
		when(agls.getGameById(1L)).thenReturn(game);
		when(game.getGameEngine()).thenReturn(gameEngine);
		when(gameEngine.getId()).thenReturn(1L);
	    
		servlet.doGet(request, response);
		
		verify(rd).forward(request, response);
		verify(session).getAttribute("player");
		verify(agls).getGameById(1L);
		verify(agls).getPlayingGameIDs(second);
		verify(agls).getCreatedGameIDs(second);
		verify(agls).getWaitingGameIDs(second);
		
	}

}
