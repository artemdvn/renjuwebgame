package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.lang.reflect.Field;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.controllers.RegisterNewPlayerServlet;
import com.softserveinc.ita.multigame.web.services.PlayerService;

public class TestRegisterNewPlayerServlet extends Mockito{
	
	@Mock
	private HttpServletRequest request;	
	@Mock
	private HttpServletResponse response;	
	@Mock
	private RequestDispatcher rd;	
	@Mock
    private PlayerService ps;
	
	private RegisterNewPlayerServlet servlet;
	
	private Player second;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		second = ps.getPlayerByNameAndPassword("Second", "second");
		if (second == null) {
			ps.registerNewPlayer("Second", "second");
			second = ps.getPlayerByNameAndPassword("Second", "second");
		}
		
		servlet = new RegisterNewPlayerServlet();
		Field f = RegisterNewPlayerServlet.class.getDeclaredField("ps");
	    f.setAccessible(true);
	    f.set(servlet, ps);	    
	}
	
	@Test
	public void testRegisterNewPlayer() throws ServletException, IOException {
		
		when(request.getParameter("user")).thenReturn("New player");
		when(request.getParameter("password")).thenReturn("new");
		when(ps.getPlayerByNameAndPassword("New player", "new")).thenReturn(second);
		when(request.getRequestDispatcher("register.jsp")).thenReturn(rd);
		
		servlet.doPost(request, response);
		
		verify(ps).getPlayerByNameAndPassword("New player", "new");
		verify(ps).registerNewPlayer("New player", "new");
		verify(request).getParameter("user");
		verify(request).getParameter("password");
		
	}

}
