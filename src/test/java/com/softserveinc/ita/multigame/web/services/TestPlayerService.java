package com.softserveinc.ita.multigame.web.services;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

import com.softserveinc.ita.multigame.dao.PlayerDao;
import com.softserveinc.ita.multigame.model.Player;

public class TestPlayerService {

	private PlayerService ps;
	private PlayerDao mockedPlayerDao;
	
	private Player rightPlayer = new Player("Right", "right");
	private Player emptyPlayer;
	private Player nullPlayer = null;

	@Before
	public void setUp() {
		mockedPlayerDao = mock(PlayerDao.class);
		ps = new PlayerService();
		ps.setPlayerDao(mockedPlayerDao);
	}

	@Test
	public void registerRightPlayer() {
		rightPlayer = ps.getPlayerByNameAndPassword("right", "right");
		if (rightPlayer == null) {
			ps.registerNewPlayer("right", "right");
			rightPlayer = ps.getPlayerByNameAndPassword("right", "right");
		}
        verify(mockedPlayerDao).create(anyObject());
	}

	@Test
	public void registerNullPlayer() {
		ps.registerNewPlayer(null, "");
		assertFalse(ps.contains(nullPlayer));
	}

	@Test
	public void registerEmptyPlayer() {
		ps.registerNewPlayer("", "");
		emptyPlayer = ps.getPlayerByNameAndPassword("", "");
		assertFalse(ps.contains(emptyPlayer));
	}

	@Test
	public void getExistingPlayerByNameAndPassword() {
		ps.getPlayerByNameAndPassword("right", "right");
		verify(mockedPlayerDao).getAll();
	}

	@Test
	public void getWrongPlayerByName() {
		assertNull(ps.getPlayerByNameAndPassword("Wrong player", ""));
	}

	@Test
	public void getExistingPlayerById() {
		ps.getPlayerById(1L);
		verify(mockedPlayerDao).get(1L);
	}

	@Test
	public void getWrongPlayerById() {
		assertNull(ps.getPlayerById(-3L));
	}

}
