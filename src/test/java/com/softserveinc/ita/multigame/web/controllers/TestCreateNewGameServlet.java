package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.lang.reflect.Field;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.controllers.CreateNewGameServlet;
import com.softserveinc.ita.multigame.web.services.AllGameListsService;

public class TestCreateNewGameServlet extends Mockito{
	
	@Mock
	private HttpServletRequest request;	
	@Mock
	private HttpServletResponse response;	
	@Mock
	private HttpSession session;	
	@Mock
	private RequestDispatcher rd;	
	@Mock
    private AllGameListsService agls;
	@Mock
	private Player second;
	
	private CreateNewGameServlet servlet;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		servlet = new CreateNewGameServlet();
	    Field f = CreateNewGameServlet.class.getDeclaredField("agls");
	    f.setAccessible(true);
	    f.set(servlet, agls);
	}
	
	@Test
	public void testCreateNewGame() throws ServletException, IOException {
		
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("player")).thenReturn(second);
		when(request.getRequestDispatcher("/GameListController")).thenReturn(rd);
	    
		servlet.doPost(request, response);
		
		verify(rd).forward(request, response);
		verify(session).getAttribute("player");
		verify(agls).createGame(second);
		verify(agls).getPlayingGameIDs(second);
		verify(agls).getCreatedGameIDs(second);
		verify(agls).getWaitingGameIDs(second);
		
	}

}
