package com.softserveinc.ita.multigame.web.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.softserveinc.ita.multigame.dao.GameHistoryDao;
import com.softserveinc.ita.multigame.model.GameHistory;

public class TestGameHistoryService {

    private GameHistoryService gameHistoryService;
    private GameHistoryDao mockedGameHistoryDao;
    private GameHistory gameHistory;

    @Before
    public void setUp() {
        mockedGameHistoryDao = mock(GameHistoryDao.class);
        gameHistoryService = new GameHistoryService();
        gameHistoryService.setGameHistoryDao(mockedGameHistoryDao);
    }

    @Test
    public void testCreateGameHistory() {
    	gameHistory = mock(GameHistory.class);    	
        gameHistoryService.create(gameHistory);
        verify(mockedGameHistoryDao).create(gameHistory);
    }

    @Test
    public void testUpdateGameHistory() {
        gameHistory = mock(GameHistory.class);
        gameHistoryService.update(gameHistory);
        verify(mockedGameHistoryDao).update(gameHistory);
    }

    @Test
    public void testDeleteGameHistory() {
        gameHistoryService.delete(99L);
        verify(mockedGameHistoryDao).delete(99L);
    }

    @Test
    public void testGetGameHistoryById() {
        when(mockedGameHistoryDao.get(99L)).thenReturn(gameHistory);
        assertEquals(gameHistory, gameHistoryService.get(99L));
    }

    @Test
    public void testGetAllGameHistoriesForPlayer() {
        GameHistory anotherGameHistory = mock(GameHistory.class);
        List<GameHistory> histories = new ArrayList<>();
        histories.add(gameHistory);
        histories.add(anotherGameHistory);
        when(mockedGameHistoryDao.getHistoriesByPlayerId(1L)).thenReturn(histories);

        gameHistoryService.getPlayerGames(1L);

        verify(mockedGameHistoryDao).getHistoriesByPlayerId(1L);
        assertEquals(histories, gameHistoryService.getPlayerGames(1L));
    }


}
