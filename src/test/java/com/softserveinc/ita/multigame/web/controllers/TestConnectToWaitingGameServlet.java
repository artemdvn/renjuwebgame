package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.lang.reflect.Field;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.PlayingGame;
import com.softserveinc.ita.multigame.web.controllers.ConnectToWaitingGameServlet;
import com.softserveinc.ita.multigame.web.services.AllGameListsService;

public class TestConnectToWaitingGameServlet extends Mockito{
	
	@Mock
	private HttpServletRequest request;	
	@Mock
	private HttpServletResponse response;	
	@Mock
	private HttpSession session;	
	@Mock
	private RequestDispatcher rd;	
	@Mock
    private AllGameListsService agls;
	@Mock
	private Player second;
	@Mock
	private PlayingGame game;
	
	private ConnectToWaitingGameServlet servlet;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		servlet = new ConnectToWaitingGameServlet();
	    Field f = ConnectToWaitingGameServlet.class.getDeclaredField("agls");
	    f.setAccessible(true);
	    f.set(servlet, agls);
	}
	
	@Test
	public void testConnectToWaitingGame() throws ServletException, IOException {
		
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("player")).thenReturn(second);
		when(request.getParameter("gameID")).thenReturn("1");
	    when(request.getRequestDispatcher("playing_game_profile.jsp")).thenReturn(rd);
	    when(agls.connectToWaitingGame(1L, second)).thenReturn(game);
	    
		servlet.doPost(request, response);
		
		verify(rd).forward(request, response);
		verify(session).getAttribute("player");
		verify(request).getParameter("gameID");
		verify(agls).connectToWaitingGame(1L, second);
		
	}

}
