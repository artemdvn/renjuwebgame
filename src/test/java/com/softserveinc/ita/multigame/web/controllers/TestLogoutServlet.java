package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class TestLogoutServlet extends Mockito{
	
	@Mock
	private HttpServletRequest request;	
	@Mock
	private HttpServletResponse response;	
	@Mock
	private HttpSession session;	
	@Mock
	private RequestDispatcher rd;	
	
	private LogoutServlet servlet;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		servlet = new LogoutServlet();
		
	}
	
	@Test
	public void testLogoutDoGet() throws ServletException, IOException {
		
		when(request.getSession()).thenReturn(session);
		when(request.getRequestDispatcher("index.jsp")).thenReturn(rd);
	    
		servlet.doGet(request, response);
		
		verify(request).getSession(false);
		verify(rd).forward(request, response);
		
	}

}
