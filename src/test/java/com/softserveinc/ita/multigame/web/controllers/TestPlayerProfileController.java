package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.web.controllers.PlayerProfileController;
import com.softserveinc.ita.multigame.web.services.GameHistoryService;
import com.softserveinc.ita.multigame.web.services.PlayerService;

public class TestPlayerProfileController extends Mockito{
	
	@Mock
	private HttpServletRequest request;	
	@Mock
	private HttpServletResponse response;	
	@Mock
	private RequestDispatcher rd;	
	@Mock
    private PlayerService ps;
	@Mock
    private GameHistoryService ghs;
	@Mock
	private Player second;
	
	private PlayerProfileController servlet;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		servlet = new PlayerProfileController();
	    Field f = PlayerProfileController.class.getDeclaredField("ps");
	    f.setAccessible(true);
	    f.set(servlet, ps);
	    f = PlayerProfileController.class.getDeclaredField("ghs");
	    f.setAccessible(true);
	    f.set(servlet, ghs);
	}
	
	@Test
	public void testPlayerProfileDoGet() throws ServletException, IOException {
		
		when(request.getParameter("id")).thenReturn("0");
		when(ps.getPlayerById(0L)).thenReturn(second);
		when(request.getRequestDispatcher("player_profile.jsp")).thenReturn(rd);
		when(ghs.getPlayerGames(0L)).thenReturn(new ArrayList<GameHistory>());
	    
		servlet.doGet(request, response);
		
		verify(rd).forward(request, response);
		verify(ps).getPlayerById(0L);
		verify(request).getParameter("id");
		verify(ghs).getPlayerGames(0L);
		
	}
	
	@Test
	public void testPlayerProfileDoPost() throws ServletException, IOException {
		
		when(request.getParameter("id")).thenReturn("0");
		when(request.getParameter("email")).thenReturn("second@gmail.com");
		when(request.getParameter("fullName")).thenReturn("Second Player");
		when(request.getParameter("gender")).thenReturn("MALE");
		when(ps.getPlayerById(0L)).thenReturn(second);
		when(request.getRequestDispatcher("player_profile.jsp")).thenReturn(rd);
		when(ghs.getPlayerGames(0L)).thenReturn(new ArrayList<GameHistory>());
	    
		servlet.doPost(request, response);
		
		verify(rd).forward(request, response);
		verify(ps, atLeastOnce()).getPlayerById(0L);
		verify(request, atLeastOnce()).getParameter("id");
		verify(request).getParameter("email");
		verify(request).getParameter("fullName");
		verify(request).getParameter("gender");
		verify(ghs).getPlayerGames(0L);
		
	}

}
