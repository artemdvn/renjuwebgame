package com.softserveinc.ita.multigame.web.services;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.softserveinc.ita.multigame.model.GameListManager;
import com.softserveinc.ita.multigame.model.Player;

public class TestAllGameListService {
	
	private Player first = new Player("First", "first");
	private Player second = new Player("Second", "second");
	private GameListManager glm = GameListManager.getInstance();
	private AllGameListsService agls = new AllGameListsService();
	
	@Before
	public void setUp() {
		glm.createGame(first);
	}
	
	@Test
	public void testConnectToWaitingGame() {
		
		List<Long> ls = glm.getWaitingGameIDs(second);
		for (Long id : ls) {
			agls.connectToWaitingGame(id, second);
		}
		ls = glm.getWaitingGameIDs(second);
		assertEquals(0, ls.size());
		
	}
	
}
