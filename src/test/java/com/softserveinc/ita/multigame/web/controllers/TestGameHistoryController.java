package com.softserveinc.ita.multigame.web.controllers;

import java.io.IOException;
import java.lang.reflect.Field;
import java.time.LocalDateTime;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.ResultStatus;
import com.softserveinc.ita.multigame.web.services.GameHistoryService;

public class TestGameHistoryController extends Mockito{
	
	@Mock
	private HttpServletRequest request;	
	@Mock
	private HttpServletResponse response;	
	@Mock
	private HttpSession session;	
	@Mock
	private RequestDispatcher rd;	
	@Mock
    private GameHistoryService ghs;
	@Mock
    private GameHistory game;
	@Mock
	private Player second;
	
	private GameHistoryController servlet;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		servlet = new GameHistoryController();
	    Field f = GameHistoryController.class.getDeclaredField("ghs");
	    f.setAccessible(true);
	    f.set(servlet, ghs);
	}
	
	@Test
	public void testGameHistoryControllerDoGet() throws ServletException, IOException {
		
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("player")).thenReturn(second);
		when(request.getParameter("id")).thenReturn("1");
		when(request.getRequestDispatcher("game_history.jsp")).thenReturn(rd);
		when(ghs.get(1L)).thenReturn(game);
		when(game.getResultStatus()).thenReturn(ResultStatus.FIRST_PLAYER_WINS);
		when(game.getFirstPlayer()).thenReturn(second);
		when(game.getStartTime()).thenReturn(LocalDateTime.now());
		when(game.getEndTime()).thenReturn(LocalDateTime.now());
		
		servlet.doGet(request, response);
		
		verify(rd).forward(request, response);
		verify(session).getAttribute("player");
		verify(ghs).get(1L);
		verify(game).getResultStatus();
		verify(game).getFirstPlayer();
		verify(game).getStartTime();
		verify(game).getEndTime();
		
	}

}
