package com.softserveinc.ita.multigame.dao;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.softserveinc.ita.multigame.model.Player;

@Category(IntegrationTest.class)
public class TestPlayerDao {

    private PlayerDao playerDao = PlayerDaoImpl.getInstance();
    private SessionFactory hsqlsf = HsqlUtil.getSessionFactory();
    private SessionFactory sessionFactoryMock = mock(SessionFactory.class);
    
    private Player first = new Player("First", "first");
    private Player second = new Player("Second", "second");
    
    @Before
    public void setUp() {
    	try {
    		Field field = playerDao.getClass().getDeclaredField("sf");
            field.setAccessible(true);
            field.set(playerDao, sessionFactoryMock);
        } catch (Exception e) {
            fail("Field \"sf\" is not accessible");
        }    	
    }

    @Test
    public void testCreatePlayer() throws Exception {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        playerDao.create(first);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.create(second);
    	
    	verify(sessionFactoryMock, times(2)).openSession();
    }
    
    @Test
    public void testGetPlayerById() throws Exception {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        playerDao.create(first);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.create(second);
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        assertThat(playerDao.get(1L).getLogin(), is(first.getLogin()));
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        assertThat(playerDao.get(2L).getLogin(), is(second.getLogin()));
        
        verify(sessionFactoryMock, times(4)).openSession();
    }
    
    @Test
    public void testGetAllPlayers() {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        playerDao.create(first);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.create(second);
    	
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        Player first = playerDao.get(1L);
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        List<Player> allPlayers = playerDao.getAll();
        assertTrue(allPlayers.contains(first));
        
        verify(sessionFactoryMock, times(4)).openSession();

    }
    
    @Test
    public void testUpdatePlayer() throws Exception {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        playerDao.create(first);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.create(second);    	
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	Player p1 = playerDao.get(first.getId());
    	p1.setFullName("First player");
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        playerDao.update(p1);
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        Player p2 = playerDao.get(second.getId());
    	p2.setFullName("Second player");
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.update(p2);
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	assertTrue(playerDao.get(first.getId()).getFullName().equals("First player"));
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	assertTrue(playerDao.get(second.getId()).getFullName().equals("Second player"));
    	
    	verify(sessionFactoryMock, times(8)).openSession();
    }
    
    @Test
    public void testDeletePlayer() throws Exception {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        playerDao.create(first);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.create(second);    	
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        playerDao.delete(first);
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.delete(second);
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	assertNull(playerDao.get(first.getId()));
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	assertNull(playerDao.get(second.getId()));
    	
    	verify(sessionFactoryMock, times(6)).openSession();
    }
    
    @Test
    public void testDeletePlayerById() throws Exception {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        playerDao.create(first);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.create(second);    	
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.delete(first.getId());
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.delete(second.getId());
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	assertNull(playerDao.get(first.getId()));
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	assertNull(playerDao.get(second.getId()));
    	
    	verify(sessionFactoryMock, times(6)).openSession();
    }
    
}
