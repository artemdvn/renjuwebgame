package com.softserveinc.ita.multigame.dao;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.softserveinc.ita.multigame.model.GameHistory;
import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.ResultStatus;

@Category(IntegrationTest.class)
public class TestGameHistoryDao {

    private GameHistoryDao gameHistoryDao = GameHistoryDao.getInstance();
    private PlayerDao playerDao = PlayerDaoImpl.getInstance();
    private SessionFactory hsqlsf = HsqlUtil.getSessionFactory();
    private SessionFactory sessionFactoryMock = mock(SessionFactory.class);
    
    private Player first = new Player("First", "first");
    private Player second = new Player("Second", "second");
    private GameHistory firstGameHistory = new GameHistory(1L, first, second, LocalDateTime.now(), LocalDateTime.now(), new ArrayList<>(), ResultStatus.SECOND_PLAYER_WINS);
    private GameHistory secondGameHistory = new GameHistory(2L, second, first, LocalDateTime.now(), LocalDateTime.now(), new ArrayList<>(), ResultStatus.FIRST_PLAYER_WINS);
    
    @Before
    public void setUp() {
    	try {
    		Field field = gameHistoryDao.getClass().getDeclaredField("sf");
            field.setAccessible(true);
            field.set(gameHistoryDao, sessionFactoryMock);
        } catch (Exception e) {
            fail("Field \"sf\" is not accessible");
        }  
    	
    	try {
    		Field field = playerDao.getClass().getDeclaredField("sf");
            field.setAccessible(true);
            field.set(playerDao, sessionFactoryMock);
        } catch (Exception e) {
            fail("Field \"sf\" is not accessible");
        }
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        playerDao.create(first);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	playerDao.create(second);
    }

    @Test
    public void testCreateGameHistory() throws Exception {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(firstGameHistory);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(secondGameHistory);
    	
    	verify(sessionFactoryMock, times(4)).openSession();
    }
    
    @Test
    public void testGetGameHistoryById() throws Exception {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(firstGameHistory);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(secondGameHistory);
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        assertThat(gameHistoryDao.get(firstGameHistory.getId()).getId(), is(firstGameHistory.getId()));
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        assertThat(gameHistoryDao.get(secondGameHistory.getId()).getId(), is(secondGameHistory.getId()));
        
        verify(sessionFactoryMock, times(6)).openSession();
    }
    
    @Test
    public void testGetAllGameHistories() {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(firstGameHistory);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(secondGameHistory);
    	
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        GameHistory gh1 = gameHistoryDao.get(firstGameHistory.getId());
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        List<GameHistory> allGameHistories = gameHistoryDao.getAll();
        assertTrue(allGameHistories.contains(gh1));
        
        verify(sessionFactoryMock, times(6)).openSession();

    }
    
    @Test
    public void testGetHistoriesByPlayerId() {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(firstGameHistory);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(secondGameHistory);
    	
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        List<GameHistory> gameHistories1 = gameHistoryDao.getHistoriesByPlayerId(first.getId());
        assertEquals(gameHistories1.size(), 2);
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        List<GameHistory> gameHistories2 = gameHistoryDao.getHistoriesByPlayerId(second.getId());
        assertEquals(gameHistories2.size(), 2);
        
        verify(sessionFactoryMock, times(6)).openSession();

    }
    
    @Test
    public void testUpdateGameHistory() throws Exception {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(firstGameHistory);
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	GameHistory gh1 = gameHistoryDao.get(firstGameHistory.getId());
    	
    	List<String> turnList = new ArrayList<>();
    	turnList.add("turn 1");
    	turnList.add("turn 2");
    	gh1.setTurnList(turnList);
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.update(gh1);
        
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	List<String> tl = gameHistoryDao.get(firstGameHistory.getId()).getTurnList();
    	assertTrue(tl.get(0).equals(turnList.get(0)));
    	
    	verify(sessionFactoryMock, times(6)).openSession();
    }
    
    @Test
    public void testDeleteGameHistoryById() throws Exception {
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(firstGameHistory);
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.create(secondGameHistory);    	
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	gameHistoryDao.delete(1L);
        when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
        gameHistoryDao.delete(2L);
    	
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	assertNull(gameHistoryDao.get(1L));
    	when(sessionFactoryMock.openSession()).thenReturn(hsqlsf.openSession());
    	assertNull(gameHistoryDao.get(2L));
    	
    	verify(sessionFactoryMock, times(8)).openSession();
    }
    
}
