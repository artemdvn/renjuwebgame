package com.softserveinc.ita.multigame.model.engine.renju;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameResultCode;

public class TestRenjuGameValidateTurnLogic {
	
	RenjuGame game = new RenjuGame();
	
	@Before
	public void setUp() {
		game.setFirstPlayer(new Player("Petro", "petro"));
		game.setSecondPlayer(new Player("Mykola", "mykola"));
	}

	@Test
	public void rightTurnOfTheFirstPlayer() {
		game.makeTurn(game.getFirstPlayer(), "3,3");
		assertEquals(game.getResultCode(), GameResultCode.OK);
	}
	
	@Test
	public void rightTurnOfTheSecondPlayer() {
		game.makeTurn(game.getFirstPlayer(), "1,1");
		game.makeTurn(game.getSecondPlayer(), "2,12");
		assertEquals(game.getResultCode(), GameResultCode.OK);
	}
	
	@Test
	public void wrongTurnOfTheFirstPlayer() {
		game.makeTurn(game.getFirstPlayer(), "1,1");
		game.makeTurn(game.getSecondPlayer(), "3,8");
		game.makeTurn(game.getFirstPlayer(), "1,1");
		assertEquals(game.getResultCode(), GameResultCode.BAD_TURN_LOGIC);
	}
	
	@Test
	public void wrongTurnOfTheSecondPlayer() {
		game.makeTurn(game.getFirstPlayer(), "1,1");
		game.makeTurn(game.getSecondPlayer(), "2,12");
		game.makeTurn(game.getFirstPlayer(), "1,2");
		game.makeTurn(game.getSecondPlayer(), "1,2");
		assertEquals(game.getResultCode(), GameResultCode.BAD_TURN_LOGIC);
	}

}
