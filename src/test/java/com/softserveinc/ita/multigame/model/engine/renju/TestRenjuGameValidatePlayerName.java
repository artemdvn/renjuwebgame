package com.softserveinc.ita.multigame.model.engine.renju;

import static org.junit.Assert.*;

import org.junit.Test;

import com.softserveinc.ita.multigame.model.Player;

public class TestRenjuGameValidatePlayerName {
	
	RenjuGame game = new RenjuGame();

	@Test
	public void rightName() {
		assertTrue(game.validatePlayer(new Player("Ivan", "ivan")));
	}

	@Test
	public void anotherRightName() {
		assertTrue(game.validatePlayer(new Player("vte?,vr.", "vte")));
	}

	@Test
	public void nullName() {
		assertFalse(game.validatePlayer(null));
	}

	@Test
	public void emptyName() {
		assertFalse(game.validatePlayer(new Player("", "")));
	}
	
}
