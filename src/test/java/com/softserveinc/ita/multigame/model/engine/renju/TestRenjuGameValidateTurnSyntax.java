package com.softserveinc.ita.multigame.model.engine.renju;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.softserveinc.ita.multigame.model.Player;
import com.softserveinc.ita.multigame.model.engine.GameResultCode;

public class TestRenjuGameValidateTurnSyntax {
	
	RenjuGame game = new RenjuGame();
	
	@Before
	public void setUp() {
		game.setFirstPlayer(new Player("Ivan", "ivan"));
		game.setSecondPlayer(new Player("Mykola", "mykola"));
	}

	@Test
	public void nullTurn() {
		game.makeTurn(game.getFirstPlayer(), null);
		assertEquals(game.getResultCode(), GameResultCode.BAD_TURN_SYNTAX);
	}
	
	@Test
	public void wrongTurnOfTheFirstPlayer() {
		game.makeTurn(game.getFirstPlayer(), "12sa");
		assertEquals(game.getResultCode(), GameResultCode.BAD_TURN_SYNTAX);
	}
	
	@Test
	public void zeroTurnOfTheFirstPlayer() {
		game.makeTurn(game.getFirstPlayer(), "0");
		assertEquals(game.getResultCode(), GameResultCode.BAD_TURN_SYNTAX);
	}
	
	@Test
	public void rightTurnOfTheFirstPlayer() {
		game.makeTurn(game.getFirstPlayer(), "0,2");
		assertEquals(game.getResultCode(), GameResultCode.OK);
	}
	
	@Test
	public void rightTurnOfTheSecondPlayer() {
		game.makeTurn(game.getFirstPlayer(), "0,2");
		game.makeTurn(game.getSecondPlayer(), "1,1");
		assertEquals(game.getResultCode(), GameResultCode.OK);
	}
	
	@Test
	public void wrongTurnOfTheSecondPlayer() {
		game.makeTurn(game.getFirstPlayer(), "0,2");
		game.makeTurn(game.getSecondPlayer(), "-1,3");
		assertEquals(game.getResultCode(), GameResultCode.BAD_TURN_SYNTAX);
	}
	
	@Test
	public void rightTurnAfterWrongTurnOfTheSecondPlayer() {
		game.makeTurn(game.getFirstPlayer(), "0,2");
		game.makeTurn(game.getSecondPlayer(), "1,22");
		assertEquals(game.getResultCode(), GameResultCode.BAD_TURN_SYNTAX);
		game.makeTurn(game.getSecondPlayer(), "-11,7");
		assertEquals(game.getResultCode(), GameResultCode.BAD_TURN_SYNTAX);
		game.makeTurn(game.getSecondPlayer(), "1,3");
		assertEquals(game.getResultCode(), GameResultCode.OK);
	}

}
