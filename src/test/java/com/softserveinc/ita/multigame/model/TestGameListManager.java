package com.softserveinc.ita.multigame.model;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TestGameListManager {

	private GameListManager glm = GameListManager.getInstance();
	
	private Player first = new Player("First", "first");
	private Player second = new Player("Second", "second");

	@Before
	public void setUp() {
		glm.createGame(first);
		glm.createGame(second);
		PlayingGame playingGame = glm.createGame(first);
		playingGame.getGameEngine().setSecondPlayer(second);
		playingGame.setStartTime();
	}

	@Test
	public void testGetCreatedGameIDs() {
		List<Long> ls = glm.getCreatedGameIDs(first);
		assertNotEquals(0, ls.size());
	}

	@Test
	public void testGetPlayingGameIDs() {
		List<Long> ls = glm.getPlayingGameIDs(first);
		assertNotEquals(0, ls.size());
	}

	@Test
	public void testGetWaitingGameIDs() {
		List<Long> ls = glm.getWaitingGameIDs(first);
		assertNotEquals(0, ls.size());
	}

}
